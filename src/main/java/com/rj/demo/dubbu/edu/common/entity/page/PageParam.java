package com.rj.demo.dubbu.edu.common.entity.page;

import java.io.Serializable;

/**
 * 
 * @ClassName: PageParam
 * @Description: TODO(用一句话描述这个类的作用)
 * 分页参数传递工具类
 * @author rj 597306518@qq.com
 * @data 2017年8月2日 下午11:51:17
 *
 */
public class PageParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9145361770542551325L;
	
	//当前页数
	private int pageNum;
	
	//每页记录数
	private int numPerPage;

	public PageParam() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PageParam(int pageNum, int numPerPage) {
		super();
		this.pageNum = pageNum;
		this.numPerPage = numPerPage;
	}
	
	

}
