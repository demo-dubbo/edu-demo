package com.rj.demo.dubbu.edu.user.service.impl;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rj.demo.dubbu.edu.common.entity.page.PageBean;
import com.rj.demo.dubbu.edu.common.entity.page.PageParam;
import com.rj.demo.dubbu.edu.user.entity.User;
import com.rj.demo.dubbu.edu.user.mapper.UserMapper;
import com.rj.demo.dubbu.edu.user.service.IUserService;

@Service
public class UserServiceImpl implements IUserService{

	@Autowired
	private UserMapper userMapper;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Transactional
	@Override
	public void save(User user) {
		int id = userMapper.insertSelective(user);
		System.out.println(id);
	}

	@Override
	public User getById(Integer userId) {
		User user = userMapper.selectByPrimaryKey(userId);
		return user;
	}

	@Override
	public User findUserByUserNo(String userNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteUserById(long userId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateUserPwd(Long userId, String newPwd, boolean isTrue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PageBean listPage(PageParam pageParam, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public static void main(String[] args) {
		UserServiceImpl userServiceImpl = new UserServiceImpl();
		User user = new User();
		user.setCreateTime(new Date());
		user.setEmail("test@test.com");
		user.setName("test");
		user.setName("test");
		user.setStatus("1");
		userServiceImpl.save(user);
	}
}
