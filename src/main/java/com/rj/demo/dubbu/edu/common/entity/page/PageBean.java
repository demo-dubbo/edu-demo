package com.rj.demo.dubbu.edu.common.entity.page;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @ClassName: PageBean
 * @Description: TODO(用一句话描述这个类的作用)
 * 分页组件
 * @author rj 597306518@qq.com
 * @data 2017年8月2日 下午11:55:05
 *
 */
public class PageBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3625986648440163282L;
	
	//当前页
	private int currentPage;
	//每页显示多少条
	private int numPerPage;
	
	//查询数据库总记录数
	private int totalCount;
	//本页数据列表
	private List<Object> recordList;
	
	//总页数
	private int pageCount;
	//页码列表的开始索引（包含）
	private int beginPageIndex;
	//页码列表的结束索引（包含）
	private int endPageIndex;
	
	
	//当前分页条件下的统计结果
	private Map<String ,Object> countResultMap;


	public PageBean() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * 只接受前四个必要条件，会自动计算出其他3各属性的值
	 * @param currentPage
	 * @param numPerPage
	 * @param totalCount
	 * @param recordList
	 */
	public PageBean(int currentPage, int numPerPage, int totalCount, List<Object> recordList) {
		super();
		this.currentPage = currentPage;
		this.numPerPage = numPerPage;
		this.totalCount = totalCount;
		this.recordList = recordList;
		
		//计算总页码
		pageCount = (totalCount + numPerPage - 1) / numPerPage;
		
		//计算beginPageIndex 和endPageIndex  ,总页数不多于10页，则全部显示
		if (pageCount <= 10) {
			beginPageIndex = 1;
			endPageIndex = pageCount;
		}
		//总页数多于10页，则显示当前页附近的共10个页码
		else{
			//当前页面附近的共10个页码（前4个 + 当前页 + 后5个）
			beginPageIndex = currentPage - 4;
			endPageIndex = currentPage + 5;
			//当前页的页码不足4个时，则显示前10个页码
			if (beginPageIndex < 1) {
				beginPageIndex = 1;
				endPageIndex = 10;
			}
			//后面的页码不足5个时，则显示后10个页码
			if (endPageIndex > pageCount) {
				endPageIndex = pageCount;
				beginPageIndex = pageCount - 10 + 1;
			}
		}
	}

	

	public PageBean(int currentPage, int numPerPage, int totalCount, List<Object> recordList, int pageCount,
			Map<String, Object> countResultMap) {
		super();
		this.currentPage = currentPage;
		this.numPerPage = numPerPage;
		this.totalCount = totalCount;
		this.recordList = recordList;
		this.pageCount = pageCount;
		this.countResultMap = countResultMap;
		//计算总页码
				pageCount = (totalCount + numPerPage - 1) / numPerPage;
				
				//计算beginPageIndex 和endPageIndex  ,总页数不多于10页，则全部显示
				if (pageCount <= 10) {
					beginPageIndex = 1;
					endPageIndex = pageCount;
				}
				//总页数多于10页，则显示当前页附近的共10个页码
				else{
					//当前页面附近的共10个页码（前4个 + 当前页 + 后5个）
					beginPageIndex = currentPage - 4;
					endPageIndex = currentPage + 5;
					//当前页的页码不足4个时，则显示前10个页码
					if (beginPageIndex < 1) {
						beginPageIndex = 1;
						endPageIndex = 10;
					}
					//后面的页码不足5个时，则显示后10个页码
					if (endPageIndex > pageCount) {
						endPageIndex = pageCount;
						beginPageIndex = pageCount - 10 + 1;
					}
				}
	}


	public int getCurrentPage() {
		return currentPage;
	}


	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}


	public int getNumPerPage() {
		return numPerPage;
	}


	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}


	public int getTotalCount() {
		return totalCount;
	}


	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}


	public List<Object> getRecordList() {
		return recordList;
	}


	public void setRecordList(List<Object> recordList) {
		this.recordList = recordList;
	}


	public int getPageCount() {
		return pageCount;
	}


	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}


	public int getBeginPageIndex() {
		return beginPageIndex;
	}


	public void setBeginPageIndex(int beginPageIndex) {
		this.beginPageIndex = beginPageIndex;
	}


	public int getEndPageIndex() {
		return endPageIndex;
	}


	public void setEndPageIndex(int endPageIndex) {
		this.endPageIndex = endPageIndex;
	}


	public Map<String, Object> getCountResultMap() {
		return countResultMap;
	}


	public void setCountResultMap(Map<String, Object> countResultMap) {
		this.countResultMap = countResultMap;
	}


	@Override
	public String toString() {
		return "PageBean [currentPage=" + currentPage + ", numPerPage=" + numPerPage + ", totalCount=" + totalCount
				+ ", recordList=" + recordList + ", pageCount=" + pageCount + ", beginPageIndex=" + beginPageIndex
				+ ", endPageIndex=" + endPageIndex + ", countResultMap=" + countResultMap + "]";
	}
	
	
	
	

}
