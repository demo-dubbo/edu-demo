package com.rj.demo.dubbu.edu.common.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @ClassName: BaseEntity
 * @Description: TODO(用一句话描述这个类的作用)
 * 基础实体
 * @author rj 597306518@qq.com
 * @data 2017年8月2日 下午11:50:19
 *
 */
public class BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1412198303282560354L;
	
	private Long id;
	private Integer version = 0;
	private Date createTime;
	
	
	public BaseEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public BaseEntity(Long id, Integer version, Date createTime) {
		super();
		this.id = id;
		this.version = version;
		this.createTime = createTime;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getVersion() {
		return version;
	}


	public void setVersion(Integer version) {
		this.version = version;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	


}
