package com.rj.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.rj.demo.dubbu.edu.user.entity.User;
import com.rj.demo.dubbu.edu.user.service.IUserService;
import com.rj.test.BaseTest;

public class TestService extends BaseTest {
	

	@Autowired
	private IUserService userService;
	
	
	@Test
	public void getById() {
		User user = userService.getById(1);
		System.out.println(user);
	}
	

}
