package com.rj.test.dao;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.rj.demo.dubbu.edu.user.entity.User;
import com.rj.demo.dubbu.edu.user.mapper.UserMapper;
import com.rj.test.BaseTest;

public class TestDao extends BaseTest {
	@Autowired
	private UserMapper userMapper;
	@Test
	public void testSave() {
		User user = new User();
		user.setCreateTime(new Date());
		user.setEmail("test@test.com");
		user.setName("test");
		user.setName("test");
		user.setStatus("1");
		userMapper.insertSelective(user);
	}
}
