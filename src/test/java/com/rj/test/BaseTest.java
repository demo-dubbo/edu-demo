package com.rj.test;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rj.demo.dubbu.edu.user.entity.User;
import com.rj.demo.dubbu.edu.user.service.IUserService;


@RunWith(SpringJUnit4ClassRunner.class)
//告诉junit spring配置文件的位置
@ContextConfiguration(locations = {"classpath:spring/spring-service.xml","classpath:spring/spring-dao.xml"})
//@ContextConfiguration(locations = {"classpath:spring/spring-dao.xml"})
public class BaseTest {
	
}
